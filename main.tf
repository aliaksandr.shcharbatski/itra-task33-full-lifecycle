resource "hcloud_ssh_key" "default" {
  name       = "hetzner_key"
  public_key = file("./id_rsa.pub")
}

resource "hcloud_server" "gitlab" {
  name        = "gitlab"
  image       = "ubuntu-20.04"
  server_type = "cx21"
  location = var.location
  ssh_keys    = [hcloud_ssh_key.default.id]
}

resource "hcloud_server" "nexus" {
  name        = "nexus"
  image       = "ubuntu-20.04"
  server_type = "cx21"
  location = var.location
  ssh_keys    = [hcloud_ssh_key.default.id]
}

resource "hcloud_server" "runner" {
  name        = "runner"
  image       = "ubuntu-20.04"
  server_type = "cx21"
  location = var.location
  ssh_keys    = [hcloud_ssh_key.default.id]
}

locals {
    servers = [
      {
        id = hcloud_server.gitlab.id
        name = hcloud_server.gitlab.name
        address = hcloud_server.gitlab.ipv4_address
      },
      {
        id = hcloud_server.nexus.id
        name = hcloud_server.nexus.name
        address = hcloud_server.nexus.ipv4_address
      },
      {
        id = hcloud_server.runner.id
        name = hcloud_server.runner.name
        address = hcloud_server.runner.ipv4_address
      },
  ]
}

resource "local_file" "ansible_inventory" {
  content = templatefile("inventory.tpl",
    {
     servers_list = local.servers
    }
  )
  filename = "ansible/inventory"
}

resource "null_resource" "ansible-run" {
  triggers = {
    instance_ids = "${join(",", local.servers.*.id)}"
  }

  provisioner "local-exec" {
    command = "ansible-playbook -T 60 -i ansible/inventory ansible/playbook.yml -v"
  }

  depends_on = [
    local_file.ansible_inventory
  ]
}