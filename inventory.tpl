[all]
%{ for server in servers_list ~}
${server.name} ansible_host=${server.address} ansible_user=root ansible_ssh_common_args='-o StrictHostKeyChecking=no'
%{ endfor ~}