# itra-task33-full-lifecycle


>Воспроизвести полный жизненный цикл разработки приложения. 
>
>1. Установить и настроить Gitlab 
>
>2. Установить и настроить Nexus (можно пропустить, если не много ресурсов)
>
>3. Выбрать приложение и залить его в ваш Gitlab (желательно что-то с Java, чтобы можно было хранить артефакты в Nexus, но можно любое на ваш выбор)
>
>4. Установить CI на ваш выбор: Jenkins, TeamCity, что-то еще. Либо можно использовать GitlabCI
>
>5. Настроить сборку приложения по событию в Gitlab (push, PR - на ваше усмотрение). Можно добавить анализ кода, например https://www.sonarqube.org
>
>6. Настроить deploy/delivery приложения

Всё развертывание в данной лабораторной работе мне захотелось сделать в автоматическом режиме, по этому пара комментариев:
- Везде далее используется terraform для развертывания серверов. Локально такого количества ресурсов у меня нет, а azure триал уже закончился, по этому использовал то что имелось под рукой - hetzner и его провайдер для тераформа.
- После развертывания серверов выполняется их настройка при помощи ansible.
- В качестве CI используется gitlab-ci
- В качестве java приложения используем https://github.com/appuio/example-spring-boot-helloworld , собирать будем при помощи gradle.


Прежде всего установим некоторые зависимости на хост с которого будем запускать развертывание всей инфраструктуры.
```
ansible-galaxy install -r requirements.yml
ansible-galaxy collection install lvrfrc87.git_acp
pip install python-gitlab
```
содержимое requirements.yml
```
- src: geerlingguy.gitlab
- src: riemers.gitlab-runner
- src: http://github.com/idealista/nexus-role.git
  scm: git
  version: 2.3.1
  name: nexus

```
Создаем сервера тераформом. Используем провайдер hetznercloud/hcloud. providers.tf:
```
terraform {
  required_providers {
    hcloud = {
      source = "hetznercloud/hcloud"
      version = "1.33.2"
    }
  }
}


provider "hcloud" {
  token   = var.hcloud_token
}

```
Api токен для доступа к хетзнеру сохраняем в terraform.tfvars и добавляем данный файл в .gitignore
```
hcloud_token = "EZq2eFbfmVlRoLqPZg...............gEsbT6CR0R3YrIxk"
```

Создаем 3 сервера - на 1 будет гитлаб, 2 - gitlab-runner для сборки артифактов и одновременно сервер на который будет деплоиться готовое приложение, и 3 - nexus для хранения артифактов.
```
resource "hcloud_ssh_key" "default" {
  name       = "hetzner_key"
  public_key = file("./id_rsa.pub")
}

resource "hcloud_server" "gitlab" {
  name        = "gitlab"
  image       = "ubuntu-20.04"
  server_type = "cx21"
  location = var.location
  ssh_keys    = [hcloud_ssh_key.default.id]
}

resource "hcloud_server" "nexus" {
  name        = "nexus"
  image       = "ubuntu-20.04"
  server_type = "cx21"
  location = var.location
  ssh_keys    = [hcloud_ssh_key.default.id]
}

resource "hcloud_server" "runner" {
  name        = "runner"
  image       = "ubuntu-20.04"
  server_type = "cx21"
  location = var.location
  ssh_keys    = [hcloud_ssh_key.default.id]
}
```
Для дальнейшей настройки серверов необходимо сгенерировать Inventory файл для ansible, будем делать это при помощи templatefile. Примерный вид шаблона: inventory.tpl
```
[all]
%{ for server in servers_list ~}
${server.name} ansible_host=${server.address} ansible_user=root ansible_ssh_common_args='-o StrictHostKeyChecking=no'
%{ endfor ~}
```
Для генерации конечного файла используем следующий код. 
```
locals {
    servers = [
      {
        id = hcloud_server.gitlab.id
        name = hcloud_server.gitlab.name
        address = hcloud_server.gitlab.ipv4_address
      },
      {
        id = hcloud_server.nexus.id
        name = hcloud_server.nexus.name
        address = hcloud_server.nexus.ipv4_address
      },
      {
        id = hcloud_server.runner.id
        name = hcloud_server.runner.name
        address = hcloud_server.runner.ipv4_address
      },
  ]
}

resource "local_file" "ansible_inventory" {
  content = templatefile("inventory.tpl",
    {
     servers_list = local.servers
    }
  )
  filename = "ansible/inventory"
}
```
И далее по полученному inventory запускаем настройку при помощи ansible. Здесь можно обратить внимание на часть triggers = {...} - использована для того чтобы тераформ понимал, что при изменении в составе серверов (пересоздании) необходимо повторно создать этот ресурс (запустить еще раз ansible-playbook).
```
resource "null_resource" "ansible-run" {
  triggers = {
    instance_ids = "${join(",", local.servers.*.id)}"
  }

  provisioner "local-exec" {
    command = "ansible-playbook -T 60 -i ansible/inventory ansible/playbook.yml -v"
  }

  depends_on = [
    local_file.ansible_inventory
  ]
}
```
Далее настройка при помощи ansible. Устанавливаем gitlab при помощи роли, добавляем некоторые настройки
```
- hosts: gitlab
  tasks:
    - name: Update apt cache
      apt:
        name: "python3-pip"
        state: present
        update_cache: yes

    - name: Install bottle python package
      pip:
        name: "{{ item }}"
      with_items:
        - python-gitlab


- hosts: gitlab
  vars_files:
    - vars/main.yml
  roles:
    - { role: geerlingguy.gitlab }
```
Сгенерируем private token для доступа к api гитлаба. Будет использован для добавления ssh ключей и для создания / настройки проекта. Возможно не самый быстрый вариант - долго думает, но более простого варианта автоматизации данной процедуры я не нашел, обычно все сводится к "зайдите в гитлаб и кликните мышкой", а я хотел автоматически - используем утилиту gitlab-rails. Также при помощи данной утилиты получаем токен для привязки раннера к гитлабу.
```
- hosts: gitlab
  tasks:
    - name: set token variable
      set_fact:
        gitlab_api_private_token=HNL89T9pS_ydW0_g2WUE

    - name: generate api token
      shell: 'gitlab-rails runner -e production "{{ generate_api_token_command }}"'
      vars:
        generate_api_token_command: "token = User.find_by_username('root').personal_access_tokens.create(scopes: [:api, :write_repository], name: 'ansible_api'); token.set_token('{{ gitlab_api_private_token }}'); token.save!"
      when: not result.stat.exists

    - name: get runner token
      shell: 'gitlab-rails runner -e production "{{ generate_api_token_command }}"'
      vars:
        generate_api_token_command: "puts Gitlab::CurrentSettings.current_application_settings.runners_registration_token"
      register: runners_registration_token
      changed_when: false
      when: not result.stat.exists

```
На этом настройка гитлаба в целом закончена. Устанавливаем gitlab-runner на второй сервер. Для начала установим на него докер. Весь код можно увидеть в ansible/roles/docker/tasks/main.yml
```
- hosts: runner
  roles:
    - docker

```
Далее устанавливаем сам гитлаб-раннер, использовав роль, и добавив в переменные нужные параметры для создания двух раннеров (один будет shell а второй docker)
```
gitlab_runner_coordinator_url: "http://{{ hostvars['gitlab']['ansible_host'] }}"
gitlab_runner_registration_token: "{{ gitlab_runner_token }}"
gitlab_runner_runners:
  - name: 'Shell GitLab Runner'
    executor: shell
    tags:
      - shell
  
  - name: 'Docker gitlab runner'
    executor: docker
    tags: 
      - docker
    docker_image: 'ubuntu'
    docker_volumes:
      - "/var/run/docker.sock:/var/run/docker.sock"
      - "/cache"

- hosts: runner
  vars_files:
    - vars/main.yml
  roles:
    - { role: riemers.gitlab-runner }

- hosts: runner
  tasks:
    - name: add gitlab to docker-group
      ansible.builtin.user:
        name: "gitlab-runner"
        groups: "docker"
        append: true
```
Раннеры привязываются к инстансу gitlab используя токен который мы получили ранее. Далее установим nexus и создадим репозитории.
```
- hosts: nexus
  tasks:
    - name: Update apt cache
      apt:
        name: openjdk-8-jre-headless
        update_cache: yes

- hosts: nexus
  roles:
    - role: nexus 
      vars:
        nexus_host: "{{ hostvars['nexus']['ansible_host'] }}"
        nexus_repositories_maven:
          proxy:
            - name: maven-central
              remote_url: https://repo1.maven.org/maven2/
              layout_policy: permissive
          hosted:
            - name: maven-snapshots
              version_policy: snapshot
              write_policy: allow
            - name: maven-releases
              version_policy: release
              write_policy: allow_once
          group:
            - name: maven-public
              member_repos:
                - maven-central
                - maven-snapshots
                - maven-releases


```
Добавим ssh ключ для нашего пользователя в gitlab, при помощи него будем работать с репозиторием
```
- hosts: localhost
  tasks:
    - name: set token variable
      set_fact:
        gitlab_api_private_token=HNL89T9pS_ydW0_g2WUE
    - name: add ssh key
      shell: |
        curl -X POST -F "private_token={{ gitlab_api_private_token }}" -F "title=root" -F "key=ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCc1KQp0qSikl5EV9sEyz2punuiTVo4Pv5JmcgagD2fwlLN60egmuzTANSGvjJ3IWIXDOS3UGOM6cKl23L7R6L9QSrVkm9eDQ3X1akZcNXbs3xIzkDZvQS8jWw6SfVJd6jZyZ5XhDl14kdWczuMs5cg4H/UYDRNrQwlXfs4gR4p8GP9sH0WfZ5jqeLB3rlYr+xSUSYgoTdbvbwzOTLlJL7czgJnNWrUH484qilMp72puLYy16RWKdHbJVfYvg3EeyCu0G6UrEmmRO3861qSrpAu+26awuZwtN1NY7gAgu4L+yZ3qOHC83+U4qwJ1MBUf8Wjbs5eUoFqdgewmbI0XZdJ root@DESKTOP-1JRUN1L" "http://{{ hostvars['gitlab']['ansible_host'] }}/api/v4/user/keys"
```
Создадим проект в gitlab, склонировав существующий репозиторий с гитхаба
```
- hosts: gitlab
  vars_files:
    - vars/main.yml
  tasks:
    - name: Create GitLab Project
      community.general.gitlab_project:
        api_url: "{{ gitlab_external_url }}"
        api_token: "{{ gitlab_api_private_token }}"
        import_url: "https://github.com/appuio/example-spring-boot-helloworld.git"
        name: itra_demo
```
Добавим на gitlab сервер ключ для работы с репозиторием, и склонируем репозиторий который мы создали ранее в папку на gitlab сервере для того чтобы дополнить его необходимыми файлами.
```
- hosts: gitlab
  tasks:
    - name: copy key
      copy:
        src: id_rsa
        dest: /root/id_rsa
        mode: '0600'

    - name: clone repo
      ansible.builtin.git:
        repo: "git@{{ hostvars['gitlab']['ansible_host'] }}:root/itra_demo.git"
        dest: /root/itra_demo
        key_file: /root/id_rsa
        force: yes
        accept_hostkey: yes
```
Добавим в склонированную репу файлы необходимые для дальнейшей настройки - создания пайплайна, запуска приложения, и выгрузки артифактов в репозиторий.
```
- hosts: gitlab
  gather_facts: false
  tasks:
    - name: add build.gralde file
      copy:
        src: build.gradle
        dest: /root/itra_demo/build.gradle

    - name: set nexus location
      replace:
        path: /root/itra_demo/build.gradle
        regexp: 'NEXUS_IPADDR'
        replace: "{{ hostvars['nexus']['ansible_host'] }}"
    - name: add gitlab-ci file
      copy:
        src: .gitlab-ci.yml
        dest: /root/itra_demo/.gitlab-ci.yml
    - name: add gitlab-ci file
      copy:
        src: docker-compose.yml
        dest: /root/itra_demo/docker-compose.yml
```
Остановимся подробнее на этих файлах. Для выгрузки собранного приложения в репозиторий nexus добавляем в файл build.gradle следующие строки:
```
apply plugin: "maven-publish"

publishing {
  publications {
      mavenJava(MavenPublication) {
          artifact bootJar
      }
  }
  repositories {
    maven {
      credentials {
        username 'admin'
        password 'admin123'
      }
      url "http://NEXUS_IPADDR:8081/repository/maven-snapshots"
	  allowInsecureProtocol = true
    }
  }
}
```
Обновленные файлы пушим в репозиторий
```
  collections:
    - lvrfrc87.git_acp
  tasks:
    - name: SSH with private key | add file
      git_acp:
        user: root
        path: /root/itra_demo
        branch: master
        comment: Add file
        add: [ "." ]
        mode: ssh
        url: "git@{{ hostvars['gitlab']['ansible_host'] }}:root/itra_demo.git"
        ssh_params:
          accept_newhostkey: true
          key_file: "/root/id_rsa"
        user_name: root
        user_email: "lively.alex.mail@gmail.com"

```
сборка и деплой осуществляется через gitlab-ci
```
stages:
  - build
  - run

build-code-job:
  stage: build
  image: gradle
  tags:
    - docker
  script:
    - echo "And here we go again"
    - ./gradlew clean build publish -Dorg.gradle.daemon=false
  artifacts:
    paths:
      - build/libs/springboots2idemo*.jar

run-app:
  stage: run
  tags:
    - shell
  script:
    - docker-compose down || true
    - curl -u admin:admin123 -L -X GET "http://$NEXUS_IP:8081/service/rest/v1/search/assets/download?sort=version&repository=maven-snapshots&maven.groupId=ch.appuio.techlab&maven.artifactId=itra_demo&maven.extension=jar" --output app.jar
    - docker-compose up -d


```
docker-compose
```
version: "3.9"
services:
  app:
    image: fabric8/java-centos-openjdk11-jdk
    ports:
      - "8080:8080"
    command: [ "java", "-jar", "/app.jar" ]
    volumes:
      - ./app.jar:/app.jar:ro

```
Скриншоты:
![img1](img/img1.png)
![img2](img/img2.png)
![img3](img/img3.png)
![img4](img/img4.png)
![img5](img/img5.png)
![img6](img/img6.png)
![img7](img/img7.png)